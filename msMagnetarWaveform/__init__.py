from .msMagnetarWaveform import (
    fgw,
    fdotgw,
    h0,
    ht,
    waveform,
    Erot,
    Egw,
    max_eps_from_Erot,
    tau_em,
    tau_gw,
)
