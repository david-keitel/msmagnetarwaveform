""" millisecond magnetar power-law spindown waveform model

Copyright (C) 2017-2018 Paul Lasky, Karl Wette, David Keitel, Nikhil Sarin

Code for calculating gravitational waveforms for the millisecond magnetar model
with arbitrary braking index
This assumes spindown is a power law with braking index nn, and gravitational-wave
emission is at twice the spin frequency.

For details, see:
Lasky, Leris, Rowlinson and Glampedakis 2017 [doi:10.3847/2041-8213/aa79a7]
Lasky, Sarin and Sammut 2017 [LIGO-T1700408]
Sarin, Lasky, Sammut and Ashton 2018 [arXiv:1805.01481]
Zhang & Meszaros 2001 [doi:10.1086/320255]
"""

import numpy as np
import astropy.constants as cc
import astropy.units as uu

def fgw(time, fgw0, tau, nn):
    """ gravitational-wave frequency as function of time
    f(t) = fgw0 (1 + t/tau)^[1/(1-n)]
    see Eq. (2) of LIGO-T1700408

    time: time array [s]
    fgw0: initial gravitational-wave frequency [Hz]
    tau:  spindown timescale [s]
    nn:   braking index [dimensionless]
    """

    return fgw0 * (1.+ time / tau)**(1./(1-nn))

def fdotgw(time, fgw0, tau, nn):
    """ first derivative of gravitational-wave frequency as function of time
    f'(t) = fgw0 (1 + t/tau)^[-1+1/(1-n)] / ((1-n)tau)

    time: time array [s]
    fgw0: initial gravitational-wave frequency [Hz]
    tau:  spindown timescale [s]
    nn:   braking index [dimensionless]
    """

    return ( fgw0 * (1. + time / tau)**(-1.+1./(1.-nn)) ) / ((1.-nn)*tau)

def h0(time, fgw0, tau, nn, eps, dd, II):
    """ gravitational-wave amplitude as function of time
    h(t) = 4\pi^2 G II eps fgw(t)^2 / (c^4 dd)
    see Eq. (3) of LIGO-T1700408

    time: time array [s]
    fgw0: initial gravitational-wave frequency [Hz]
    tau:  spindown timescale [s]
    nn:   braking index [dimensionless]
    eps:  ellipticity of the star [dimensionless]
    dd:   distance [Mpc]
    II:   principal moment of inertia [g cm^2]
    """

    # create the frequency array
    fgw_values = fgw(time, fgw0, tau, nn)

    # get the units right:
    fgw_values *= uu.Hz
    II = II * uu.g * uu.cm**2 
    dd = dd * uu.Mpc

    consts = 4. * np.pi**2 * cc.G / cc.c**4

    # calculate the strain, and convert to cgs units
    # should be dimensionless, so this is a sanity check
    h0_values = (consts * II * eps * fgw_values**2 / dd).cgs

    return h0_values, fgw_values

def ht(time, fgw0, tau, nn, eps, dd, cosi, II):
    """ Produces the necessary ingredient time series
        for calculating a gravitational-wave strain time series:
        * phase offset dphi from phi0=phi(t=0)
        * plus-polarization amplitude ap
        * cross-polarization amplitude ax
        The actual strain h(t) needs to be computed by the caller,
        including detector information.

    time: time array [s]
    fgw0: initial gravitational-wave frequency [Hz]
    tau:  spindown timescale [s]
    nn:   braking index [dimensionless]
    eps:  ellipticity of the star [dimensionless]
    dd:   distance [Mpc]
    cosi: cosine of inclination angle
    II:   principal moment of inertia [g cm^2]
    """

    # Signal switches on at time=0
    if time < 0:
        return 0, 0, 0

    # dimensionless strain amplitude time series
    h0_values, _ = h0(time=time, fgw0=fgw0, tau=tau, nn=nn,
                      eps=eps, dd=dd, II=II)

    # phase: integral of f_gw(t)
    term1 = 2.* np.pi * tau * fgw0 * (1.-nn) / (2.-nn)
    term2 = (1.+time / tau)**((2.-nn) / (1.-nn)) - 1
    dphi = term1 * term2

    # amplitudes of the two polarization components
    ap = h0_values * (1. + cosi**2) / 2.   # a_plus
    ax = h0_values * cosi                  # a_cross

    return dphi, ap, ax

def waveform(fgw0, tau, nn, eps, dd, cosi, II):
    """ create a python function object
    which itself computes the gravitational waveform phase and amplitudes
    as a function of time
    and can be passed on to e.g. lalpulsar.simulateCW.CWSimulator

    fgw0: initial gravitational-wave frequency [Hz]
    tau:  spindown timescale [s]
    nn:   braking index [dimensionless]
    eps:  ellipticity of the star [dimensionless]
    dd:   distance [Mpc]
    cosi: cosine of inclination angle
    II:   principal moment of inertia [g cm^2]
    """

    def wf(time):
        dphi, ap, ax = ht(time=time, fgw0=fgw0, tau=tau, nn=nn,
                          eps=eps, dd=dd, cosi=cosi, II=II)
        return dphi, ap, ax

    return wf

def Erot(fgw0, II):
    """ initial total rotational energy
    see Eq. (11) of Sarin et al. [arXiv:1805.01481]

    fgw0: initial gravitational-wave frequency [Hz]
    II:   principal moment of inertia [g cm^2]
    """

    # get the units right:
    fgw0 = fgw0 / uu.s
    II = II * uu.g * uu.cm**2

    Erot_value = 0.5 * II * fgw0**2. * np.pi**2.

    return Erot_value.cgs

def Egw(t, fgw0, tau, nn, eps, II):
    """ emitted gravitational wave energy Egw(t) up to time t
    see Eq. (9) of Sarin et al. [arXiv:1805.01481]
    omitting - sign as we want emitted, not lost, energy

    t:    upper integration limit [s] (can be np.inf)
    fgw0: initial gravitational-wave frequency [Hz]
    tau:  spindown timescale [s]
    nn:   braking index [dimensionless]
    eps:  ellipticity of the star [dimensionless]
    II:   principal moment of inertia [g cm^2]
    """

    # get the units right:
    t = t * uu.s
    tau = tau * uu.s
    fgw0 = fgw0 / uu.s
    II = II * uu.g * uu.cm**2

    # workaround for n=7 singularity
    if nn==7:
        nn -= 1e-6

    prefactor = 32.*np.pi**6*cc.G/(5.*cc.c**5.)
    tau_nn_term = tau * ((nn-1.)/(nn-7.)) * ((1.+t/tau)**((7.-nn)/(1.-nn)) - 1.)
    Egw_value = prefactor * II**2. * fgw0**6. * eps**2. * tau_nn_term

    return Egw_value.cgs

def max_eps_from_Erot(fgw0, tau, nn, II):
    """ maximum energetically allowed ellipticity
    solving Erot=Egw as the maximum allowed value;
    with Erot from Eq. (11) of Sarin et al. [arXiv:1805.01481]
    and Egw = prefactor * other_Egw_terms * eps**2 * tau_nn_term
    from Eq. (9) ibid.,
    omitting the minus sign (emitted, not lost, energy)
    and at t->infty

    fgw0: initial gravitational-wave frequency [Hz]
    tau:  spindown timescale [s]
    nn:   braking index [dimensionless]
    II:   principal moment of inertia [g cm^2]
    """

    Erot_value = Erot(fgw0=fgw0, II=II)

    # in CGS units
    tau = tau * uu.s
    fgw0 = fgw0 / uu.s
    II = II * uu.g * uu.cm**2

    prefactor = 32.*np.pi**6*cc.G/(5.*cc.c**5.)
    # second factor for t->inf: ((1.+t/tau)**((7.-nn)/(1.-nn))-1.)=(0-1)
    tau_nn_term = tau*((nn-1.)/(nn-7.))*(-1.)
    other_Egw_terms = II**2. * fgw0**6.

    eps = np.sqrt ( Erot_value / (prefactor*other_Egw_terms*tau_nn_term) ).cgs
    return eps

def tau_em(frot0, Bp, II, R):
    """ spindown timescale for pure magnetic dipole
    see e.g. LIGO-T1700408 Eq. 11,
    or Zhang & Meszaros 2001 [doi:10.1086/320255], Eq. 6 l.h.s.:
    tau_em = 3c^3 II / (Bp^2 R^6 Omega0^2)
    To work in SI or cgs units, we substitute
    Bp^2->Bp^2/mu0 in this implementation.

    frot0: initial ROTATIONAL frequency [Hz], i.e. 0.5*fgw0
    Bp:    poloidal magnetic field in Gauss
    II:    principal moment of inertia [g cm^2]
    R:     neutron star radius in cm
    """

    # SI units:
    Omega0 = 2.*np.pi*(frot0/uu.s)
    II = II * uu.g * uu.cm**2
    Bp = (Bp*uu.G).to(uu.kg/(uu.A*uu.s**2))
    mu0 = cc.mu0.to(uu.kg*uu.m/(uu.A**2*uu.s**2))
    R = R*uu.cm

    tau = 3.0 * cc.c**3. * II / ( (Bp**2./mu0) * R**6. * Omega0**2. )
    return tau.cgs

def tau_gw(frot0, eps, II):
    """ spindown timescale for pure quadrupole GW
    see e.g. Zhang & Meszaros 2001 [doi:10.1086/320255], L37 inline after Eq. 6:
    tau_gw = 5c^5 / (128 G II eps^2 Omega0^4)

    frot0: initial ROTATIONAL frequency [Hz], i.e. 0.5*fgw0
    eps:   ellipticity of the star [dimensionless]
    II:    principal moment of inertia [g cm^2]
    """

    # get the units right:
    Omega0 = 2.*np.pi*(frot0/uu.s)
    II = II * uu.g * uu.cm**2

    tau = 5.0 * cc.c**5. / ( 128. * cc.G * II * eps**2. * Omega0**4. )
    return tau.cgs
