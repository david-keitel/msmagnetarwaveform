#!/usr/bin/env python

from setuptools import setup, find_packages
from os import path
import sys

here = path.abspath(path.dirname(__file__))
# Get the long description from the README file
with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="msMagnetarWaveform",
    version=0.1,
    author="Paul Lasky, Karl Wette, David Keitel, Nikhil Sarin",
    author_email="david.keitel@ligo.org",
    maintainer="David Keitel",
    maintainer_email="david.keitel@ligo.org",
    license="MIT",
    description="simple lalpulsar-based ms magnetar waveform simulator",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ligo.org/david-keitel/msmagnetarwaveform",
    packages=find_packages(),
    scripts=["bin/makeMsMagnetarData"],
    platforms="POSIX",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX",
        "Natural Language :: English",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Astronomy",
        "Topic :: Scientific/Engineering :: Physics",
    ],
    install_requires=[
        "numpy",
        "astropy",
        "lalsuite",
    ],
)
