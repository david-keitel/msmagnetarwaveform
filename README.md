# msMagnetarWaveform

simulator module extracted from [post-merger-long](https://git.ligo.org/david-keitel/post-merger-long/-/tree/master/scripts/magnetar_waveform) LVC project

Copyright (C) 2017-2018 Paul Lasky, Karl Wette, David Keitel, Nikhil Sarin

To install:
* `pip install git+ssh://git@git.ligo.org/david-keitel/msmagnetarwaveform.git`
* or `pip install git+https://git.ligo.org/david-keitel/msmagnetarwaveform` (will likely only work when made public)

Dependencies:
* numpy
* astropy
* lalsuite

Note on ephemerides:
As of LALSuite 6.76 the default for CWSimulator is to look for
`earth00-19-DE405` and `sun00-40-DE405` ephemerides files.
If you don't have a full LALSuite installation,
it is recommended to download the two larger files
* [earth00-40-DE405.dat.gz](https://git.ligo.org/lscsoft/lalsuite/raw/master/lalpulsar/lib/earth00-40-DE405.dat.gz)
* [sun00-40-DE405.dat.gz](https://git.ligo.org/lscsoft/lalsuite/raw/master/lalpulsar/lib/sun00-40-DE405.dat.gz)
instead.

(Eventually, CWSimulator will be updated to default to these.)
You can then pass relative or absolute file paths to the generator script, e.g.
```
makeMsMagnetarData --T 10 --earth-ephem earth00-40-DE405.dat.gz --sun-ephem sun00-40-DE405.dat.gz
```
